#ifndef UTILS_H
#define UTILS_H
#include <sys/time.h> 
#include <ctime>
#include <time.h>
#include <sstream>
#include <unistd.h>
#include <string>
#include <sys/stat.h>
#include <algorithm> 
#include "libSMTP/CSmtp.h"

bool send_email(std::string message, std::string srcAddr, std::string srcPasswd, std::string destAddr);

std::string &ltrim(std::string &s);

std::string &rtrim(std::string &s);

void trim(std::string *str);

bool is_int(std::string strNumber);

std::string find_parameter_from_text(std::string text, std::string parameter);

bool file_exists (const std::string& name);

std::string read_whole_file(std::string filename);

std::string get_cur_time();

#endif
