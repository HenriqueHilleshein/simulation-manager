#include <string>
#include <pthread.h>
#include <vector>
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "QueueManager.h"
#include "utils.h"

QueueManager<int> SimNumQueue;
std::string SimFileName = "";
std::string emailManagerFileName = "email_manager.txt";
pthread_mutex_t coutMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t FailNumMutex = PTHREAD_MUTEX_INITIALIZER;
int FailNum;
int MAX_FAIL_NUM = 5; 

void print_message(std::string message){
    pthread_mutex_lock(&coutMutex); 
    std::cout << get_cur_time() << message  << std::endl;
    pthread_mutex_unlock(&coutMutex); 
}

void send_email_parser(std::string message){
    std::string fileStr = read_whole_file(emailManagerFileName);
    std::string srcAddr = find_parameter_from_text(fileStr, "SOURCE_ADDR");      
    std::string srcPasswd = find_parameter_from_text(fileStr, "SOURCE_PASSWD");      
    std::string destAddr = find_parameter_from_text(fileStr, "DESTINATION_ADDR");      
    if(srcAddr == "Not Found"){
        print_message("SOURCE_ADDR not found from " + emailManagerFileName);
        return;
    }
    if(srcPasswd == "Not Found"){
        print_message("SOURCE_PASSWD not found from " + emailManagerFileName);
        return;
    }
    if(destAddr == "Not Found"){
        print_message("DESTINATION_ADDR not found from " + emailManagerFileName);
        return;
    }
    if (send_email(message, srcAddr, srcPasswd, destAddr)){
        print_message("Confirmation Email Sent");
    } else {
        print_message("It was not possible to send the confirmation email");
    }
}

void run_simulation(int SimNum, int tid){
    std::string SimNumStr = std::to_string(SimNum);
    pid_t pid = fork(); /* Create a child process */
    switch (pid) {
        case -1: /* Error */
        {
            print_message("Thread " + std::to_string(tid) + ": Fork failed");
            exit(1);
        }
        case 0: /* Child process */
        {
            /* open /dev/null for writing. It disables the output of the child */
            int fd = open("/dev/null", O_WRONLY);
            dup2(fd, 1);    /* make stdout a copy of fd (> /dev/null) */
            dup2(fd, 2);    /* ...and same with stderr */
            close(fd);      /* close fd */

            execlp("python3","python3", SimFileName.c_str(), SimNumStr.c_str(),(char*)NULL); /* Execute the program */
            print_message("Thread " + std::to_string(tid) + ": Execl failed" );
            exit(1); //It shouldn't happen
        }
        default: /* Parent process */
        {
            int  returnStatus;    
            waitpid(pid, &returnStatus, 0);  // Parent process waits here for child to terminate.
            if (returnStatus != 0) {
                print_message("Thread " + std::to_string(tid) + ": Simulation number " + SimNumStr + " failed with code " + 
                              std::to_string(returnStatus) + "! Running it again");
                
                pthread_mutex_lock(&FailNumMutex);  
                FailNum++;
                if (FailNum >= MAX_FAIL_NUM){
                    print_message("Thread " + std::to_string(tid) + ": It failed too many times to run the program " +  SimFileName.c_str() +  
                                  ". Please, check if the Python script is correct and if you are in the right environment");
                    print_message("Thread " + std::to_string(tid) + "Also, maybe you a running pro process in parallel than you can"); 
                    pthread_mutex_unlock(&FailNumMutex);
                    SimNumQueue.push(tid); // In case it was just to many process in parallel;
                    throw 1;
                }
                pthread_mutex_unlock(&FailNumMutex);
                run_simulation(SimNum, tid);
            }
            pthread_mutex_lock(&FailNumMutex);  
            FailNum = 0;
            pthread_mutex_unlock(&FailNumMutex);
        
        }
    return;
    }
}
void * run_thread(void *threadid)
{
     int tid = *((int *) threadid);
     print_message("Thread " + std::to_string(tid) + ": Started" );
     try {
         while (!SimNumQueue.empty()){
             int SimNum = SimNumQueue.get();
             print_message("Thread " + std::to_string(tid) + ": Starting simulation number " + std::to_string(SimNum));
             run_simulation(SimNum, tid);
             print_message("Thread " + std::to_string(tid) + ": Finished simulation number " + std::to_string(SimNum));
         }
         print_message("Thread " + std::to_string(tid) + ": Finished-Ended" );
     } catch (int i){
         print_message("Thread " + std::to_string(tid) + ": Forced to END!" );
     }
     return NULL;
}

void correct_use_message(std::string ProgramName){
     std::cout << "The correct use of this program is:" << std::endl;
     std::cout << ProgramName << " simulation_file.py INITIAL_VALUE END_VALUE NUM_OF_PROCESS_IN_PARALLEL" << std::endl;
     std::cout << "Example:" << std::endl;
     std::cout << ProgramName << " my_file.py 0 100 10" << std::endl;
}


int main(int argc, char *argv[]){
    std::string ProgramName = argv[0];
    if(argc == 5){
        FailNum = 0;
        SimFileName = argv[1];
        if (!file_exists(SimFileName)){
            std::cout << "Python file " + SimFileName + " does not exist" << std::endl;
            return -1;
        }
        std::string InitialValueStr = argv[2];
        std::string EndValueStr = argv[3];
        std::string NumOfProcessInParallelStr = argv[4];
        if (!is_int(InitialValueStr) || !is_int(EndValueStr), !is_int(NumOfProcessInParallelStr)){
            std::cout << "The first, second and third parameter must be integer\n" << std::endl;
            correct_use_message(ProgramName);
            return -1;
        }
        
        int InitialValue = stoi(InitialValueStr);
        int EndValue = stoi(EndValueStr);
        if (EndValue < InitialValue){
            std::cout << "The end value of the simulation must be greater or equal to to the initial value\n" << std::endl;
            correct_use_message(ProgramName); 
            return -1;
        }
        for (int i = InitialValue; i <= EndValue; i++){
            SimNumQueue.push(i);
        }
        int NumOfProcessInParallel = stoi(NumOfProcessInParallelStr);
        if (NumOfProcessInParallel < 1){
            std::cout << "The number of processes must be greater than 0\n" << std::endl;
            correct_use_message(ProgramName);
            return -1; 
        }
        std::vector<pthread_t> threads(NumOfProcessInParallel);
        std::vector<int*> tidVector(NumOfProcessInParallel);
        for(int i=0; i < NumOfProcessInParallel; i++ ){
            int* tid = new int;
            tidVector[i] = tid;
            *tid = i;  
            int rc = pthread_create(&threads[i], NULL, run_thread, (void *)tid); 
            if (rc){
               print_message("Error:unable to create thread," + rc);
               return -1;
            }
        }
        for(int i=0; i < NumOfProcessInParallel; i++ ){
            pthread_join(threads[i],NULL);
            delete tidVector[i];
        }
        print_message("Simulation ended");
        if  (file_exists(emailManagerFileName)){
            if (FailNum != MAX_FAIL_NUM){
               std::string email_message = "Simulation ended";
               send_email_parser(email_message);
            } else {
               std::string email_message = "Simulation Failed";
               send_email_parser(email_message);
            }
        }
    }
    else {
        correct_use_message(ProgramName);
    }
    return 0;
}

