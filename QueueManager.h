#ifndef QUEUEMANAGER_H
#define QUEUEMANAGER_H
#include <pthread.h>
#include <time.h>
#include <queue> 
#include <iostream>
template<typename T>
class QueueManager {
  public:
      QueueManager();
      ~QueueManager();
      void push(T data);
      T get();
      bool empty();

  private:
      std::queue<T> _Queue;
      pthread_mutex_t _QueueMutex;
};

template<typename T>
QueueManager<T>::QueueManager(){
    pthread_mutex_init( &(_QueueMutex), NULL);
}

template<typename T>
QueueManager<T>::~QueueManager(){}

template<typename T>
void QueueManager<T>::push(T data){
    pthread_mutex_lock(&_QueueMutex); 
    _Queue.push(data);
    pthread_mutex_unlock(&_QueueMutex);
}

template<typename T>
T QueueManager<T>::get(){
    pthread_mutex_lock(&_QueueMutex);
    T data = _Queue.front();
    _Queue.pop();
    pthread_mutex_unlock(&_QueueMutex);
    return data;
}

template<typename T>
bool QueueManager<T>::empty(){
    bool empty = false;
    pthread_mutex_lock(&_QueueMutex); 
    empty =  _Queue.empty();
    pthread_mutex_unlock(&_QueueMutex);
    return empty;
} 
#endif

