#include "utils.h"


bool send_email(std::string message, std::string srcAddr, std::string srcPasswd, std::string destAddr)
{
    bool bError = false;
    try
    {
        CSmtp mail;
        mail.SetSMTPServer("smtp.gmail.com",587);
        mail.SetSecurityType(USE_TLS);
        mail.SetLogin(srcAddr.c_str());
        mail.SetPassword(srcPasswd.c_str());
        mail.SetSenderName("Simulation Manager");
        mail.SetSenderMail(srcAddr.c_str());
        mail.SetReplyTo(srcAddr.c_str());
        mail.SetSubject("Simulation Ended!");
        mail.AddRecipient(destAddr.c_str());
        mail.SetXPriority(XPRIORITY_NORMAL);
        mail.SetXMailer("The Bat! (v3.02) Professional");
        mail.AddMsgLine("Dear Boss,");
        mail.AddMsgLine("");
        mail.AddMsgLine(message.c_str());
        mail.AddMsgLine("");
        mail.AddMsgLine("BR");
        mail.AddMsgLine("Simulation Manager");
        mail.Send();
    }
    catch(ECSmtp e)
    {
        std::cout << "Email Error: " << e.GetErrorText().c_str() << ".\n";
        bError = true;
    }
    if(!bError)
        return true;
    return false;
}

std::string &ltrim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

std::string &rtrim(std::string &s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

void trim(std::string *str) {
    ltrim(rtrim(*str));
}

std::string find_parameter_from_text(std::string text, std::string parameter) {
    size_t posParameter = text.find(parameter);
    if (posParameter != std::string::npos){
        std::string aux = text.substr(posParameter + parameter.size());
        size_t posEq = aux.find("=");
        if (posEq != std::string::npos){
            size_t posEnd = aux.find("\n");
            std::string value = aux.substr(posEq + 1, posEnd - posEq -1);
            trim(&value);
            return value;
        }
         
    }
    return "Not Found";
}

bool is_int(std::string strNumber)
{
    long int test;
    std::stringstream convert(strNumber);
    if(convert >> test && !(convert >> strNumber)){
        return true;
    }
    return false;
}


bool file_exists (const std::string& name) {
    struct stat buffer;   
    return (stat (name.c_str(), &buffer) == 0); 
}

std::string read_whole_file(std::string filename) {
    std::ifstream t(filename);
    std::string str;
    t.seekg(0, std::ios::end);   
    str.reserve(t.tellg());
    t.seekg(0, std::ios::beg);
    str.assign((std::istreambuf_iterator<char>(t)),
                std::istreambuf_iterator<char>());
    return str;
}

std::string get_cur_time()
{
    char buffer[30];
    struct timeval tv;
    time_t curtime;
    gettimeofday(&tv, NULL); 
    curtime=tv.tv_sec;
    strftime(buffer,30,"[%d-%m-%Y %T.",localtime(&curtime));
    std::string datetime(buffer);
    std::ostringstream ss;
    ss <<tv.tv_usec/1000;
    std::string milliseconds = ss.str();
    if(milliseconds.length() <= 2){
        if(milliseconds.length() == 1){
            milliseconds = "00" + milliseconds;
        } else {
            milliseconds = "0" + milliseconds;
        }
    }
    datetime += milliseconds.substr(0,3) + "] ";
    return datetime;
}

