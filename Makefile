CXX := g++
BDIR := ./libSMTP/
CXXFLAGS := -std=c++11  
LIBS := -lpthread -lssl -lcrypto
OBJ := simulation_manager.o 
EXTRA_OBJ = utils.o $(BDIR)base64.o $(BDIR)CSmtp.o $(BDIR)md5.o

simulation_manager: $(OBJ) $(EXTRA_OBJ)
	$(CXX) -o $@ $^ $(LIBS) $(CFLAGS) 
clean:
	$(RM) *.o *.d simulation_manager $(EXTRA_OBJ)
